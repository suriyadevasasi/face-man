﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Face_man_OOP_PROJECT
{
    public partial class Form1 : Form
    {
        bool goup;
        bool godown;
        bool goleft;
        bool goright;

        int speed = 5;

        int ghost1 = 1;
        int ghost2 = 1;

        int ghost3x = 8;
        int ghost3y = 8;

        int score = 0;


        public Form1()
        {
            InitializeComponent();
            label2.Visible= false;
            MessageBox.Show("start");
        }

        private void keyisdown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left)
            {
                goleft = true;
                pacman.Image = Properties.Resources.Left;
            }
            if (e.KeyCode == Keys.Right)
            {
                goright = true;
                pacman.Image = Properties.Resources.Right;
            }
            if (e.KeyCode == Keys.Up)
            {
                goup = true;
                pacman.Image = Properties.Resources.Up;
            }
            if (e.KeyCode == Keys.Down)
            {
                godown = true;
                pacman.Image = Properties.Resources.down;
            }

        }

        private void keyisup(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left)
            {
                goleft = false;
            }
            if (e.KeyCode == Keys.Right)
            {
                goright = false;
            }
            if (e.KeyCode == Keys.Up)
            {
                goup = false;
            }
            if (e.KeyCode == Keys.Down)
            {
                godown = false;
            }


        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label1.Text = "Score: " + score;

            if (goleft)
            {
                pacman.Left -= speed;

            }
            if (goright)
            {
                pacman.Left += speed;

            }
            if (goup)
            {
                pacman.Top -= speed;

            }
            if (godown)
            {
                pacman.Top += speed;
            }

            redghost.Left += ghost1;
            yellowghost.Left += ghost2;

            if (redghost.Bounds.IntersectsWith(pictureBox4.Bounds))
            {
                ghost1 = -ghost1;
            }

            else if (redghost.Bounds.IntersectsWith(pictureBox3.Bounds))
            {
                ghost1 = -ghost1;
            }

            if (yellowghost.Bounds.IntersectsWith(pictureBox1.Bounds))
            {
                ghost2 = -ghost2;
            }

            else if (yellowghost.Bounds.IntersectsWith(pictureBox2.Bounds))
            {
                ghost2 = -ghost2;
            }

            foreach (Control x in this.Controls)
            {
                if (x is PictureBox && x.Tag == "wall" || x.Tag == "ghost")
                {
                    if (((PictureBox)x).Bounds.IntersectsWith(pacman.Bounds) || score == 36)
                    {
                        pacman.Left = 0;
                        pacman.Top = 25;
                        label2.Text = "GAME OVER";
                        label2.Visible = true;
                        timer1.Stop();
                    }
                }
                if (x is PictureBox && x.Tag == "coin")
                {
                    if (((PictureBox)x).Bounds.IntersectsWith(pacman.Bounds)) 
                

                {
                    this.Controls.Remove(x); 
                    score++;


                }
            }
        }

            pinkghost.Left += ghost3x;
            pinkghost.Top += ghost3y;

            if (pinkghost.Left < 1 ||
            pinkghost.Left + pinkghost.Width > ClientSize.Width - 2 ||
            (pinkghost.Bounds.IntersectsWith(pictureBox4.Bounds)) ||
            (pinkghost.Bounds.IntersectsWith(pictureBox3.Bounds)) ||
            (pinkghost.Bounds.IntersectsWith(pictureBox1.Bounds)) ||
            (pinkghost.Bounds.IntersectsWith(pictureBox2.Bounds))
            )
            {
                ghost3x = -ghost3x;
            }
            if (pinkghost.Top < 1 || pinkghost.Top + pinkghost.Height > ClientSize.Height - 2)
            {
                ghost3y = -ghost3y;
            }
            
        }
    }
}
        





